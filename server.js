/*! * * * * * * * * * * * * * * * * * * * *
 * EBR Server
 * @version 0.1
 * @file server.js
 * @author Maxime 'Stalker2106' Martens
 * @brief File holding basic api operations
 * * * * * * * * * * * * * * * * * * * * */

//Inject modules
const express = require("express");
const bodyParser = require('body-parser');
const cors = require('cors');
const JsonDB = require('node-json-db');
const sha256 = require('sha256');


// API Param
const SERVER_PORT = 1337;
const EXPIRE_TIME = 3600000;

// DB Param
const DATABASE = "data/gamedata"; // .json

//
// HELPERS
//

function verifyAuthorization(req, res) {
  var reqToken = req.header("Authorization");
  if (!reqToken)
  {
    res.status(401).send("Missing 'Authorization' header.");
    return (false);
  }
  reqToken = reqToken.replace("Bearer ", "");
  for (var i in sessions)
  {
    if (sessions[i].token == reqToken) //Token found!
    {
      if (Date.now - sessions[i].issued >= EXPIRE_TIME)
      {
        if (sessions[i] == reqToken) sessions.splice(i, 1); //Expire token
        return (false);
      }
      sessions[i].issued = Date.now;
      return (true);
    }
  }
  res.status(401).send("Bad token.");
  return (false);
}

function getUserId(login) {
  var players = db.getData("/players");
  for (var i in players)
  {
    if (players[i].login == login) return (i);
  }
  return (-1);
}

//
// API Handlers
//

/*!
 * @brief Dev
 * @param req : HTTP request received
 * @param res : HTTP response to send
 */
function dev(req, res) {
  if (!verifyAuthorization(req, res)) return;
  try {
    if (req.query.req) res.send(200, db.getData(req.query.req));
    else res.send(db.getData("/"));
  } catch(e) {
    res.send(500, "Oh oh: "+e);
  }
}

/*!
 * @brief Get characters data
 * @param req : HTTP request received
 * @param res : HTTP response to send
 */
function getCharacters(req, res) {
  if (!verifyAuthorization(req, res)) return;
  return (res.send(200, db.getData("/characters")));
}

/*!
 * @brief Register new user to database
 * @param req : HTTP request received
 * @param res : HTTP response to send
 */
function register(req, res) {
  var newUser = {
    login: req.body.login,
    password: req.body.password,
    character: -1
  };
  if (getUserId(newUser.login) != -1) return (res.send(403, "User already exists"));
  else
  {
    try {
      db.push("/players", [newUser], false);
    } catch(e) {
      return (res.send(500, "Oh oh: "+e));
    }
    return (res.send(200, newUser));
  }
}

/*!
 * @brief Register new user to database
 * @param req : HTTP request received
 * @param res : HTTP response to send
 * @param next : ??
 */
function login(req, res) {
  if (!req.body || !req.body.login || !req.body.password)
  {
    return (res.status(400).send("Missing fields in POST"));
  }
  var playerId = getUserId(req.body.login);
  if (playerId == -1)
  {
    return (res.status(404).send("User does not exists"));
  }
  try {
    var player = db.getData("/players/"+playerId);
  } catch(e) {
    return (res.status(500).send("Oh oh: "+e));
  }
  if (player.password != req.body.password)
  {
    return (res.status(403).send("Wrong credentials"));
  }
  var genToken = sha256(playerId + new Date().toLocaleTimeString());
  sessions.push({ token: genToken, issued: Date.now()});
  res.send(200, {
    access_token : genToken,
    player : {
      login: player.login,
      character: player.character
    }
  });
}

//Server creation
var db;
var sessions = [];
var server = express();
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());
server.use(cors());

//
// Handlers binding
//

server.get("/dev", dev);
server.get("/characters", getCharacters);
server.post("/register", register);
server.post("/login", login);

//Start
db = new JsonDB(DATABASE, true, false);
console.log("Database loaded.");
server.listen(SERVER_PORT);
console.log("%s listening on %s", server.name, SERVER_PORT);
