/* global game */
/**
 * Card class
 */
class Card {

  constructor(data) {
    //Graphics
    this.sprite = game.add.sprite(100, 450, "card");
    this.sprite.animations.add("hidden", [0], 1);
    this.sprite.animations.add("visible", [1], 1);
    this.sprite.animations.play("hidden");
    this.sprite.inputEnabled = true;
    this.sprite.height = 200;
    this.sprite.width = 100;
    //Data
    this.data = data;
    var style = { font: "32px Courier", fill: "#00ff44" };
    this.title = game.add.text(0, 0, this.data.name, style);
  }

  bindClick(callback) {
    this.sprite.events.onInputUp.add(callback, this);
  }

  show() {
    this.sprite.animations.play("visible");
    this.title.visible = true;
  }

  hide() {
    this.sprite.animations.play("hidden");
    this.title.visible = false;
  }
}