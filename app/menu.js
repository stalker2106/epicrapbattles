/* global app, game, gui, SlickUI */
const max_credentials_length = 128;
/**
 * Main Menu
 */
function MainMenu() {
  gui.innerHTML = nunjucks.render('./app/menus/mainmenu.html');
  gui.querySelector("#register").addEventListener("click", function() {
    RegisterMenu();
  });
  gui.querySelector("#connect").addEventListener("click", function() {
    ConnectMenu();
  });
  //gui.style.display = "none";
}

/**
 * Menu to create account
 */
function RegisterMenu() {
  gui.innerHTML = nunjucks.render('./app/menus/playerForm.html');
  var loginField = gui.querySelector("input")[0];
  var passwordField = gui.querySelector("input")[1];
  document.getElementById("submit").addEventListener("click", function() {
    postPlayer("register", loginField.value, passwordField.value).then(function (res) {
      postPlayer("login", loginField.value, passwordField.value).then(function (res) {
        if (app.player.character == -1) CharacterSelectMenu();
        else GameMenu();
      });
    });
  });
  document.getElementById("back").addEventListener("click", function() {
    MainMenu();
  });
}

/**
 * Menu to connect to account
 */
function ConnectMenu() {
  gui.innerHTML = nunjucks.render('./app/menus/playerForm.html');
  var loginField =  document.getElementById("loginField");
  var passwordField =  document.getElementById("passwordField");
  document.getElementById("submit").addEventListener("click", function() {
    postPlayer("login", loginField.value, passwordField.value).then(function (res) {
      if (app.player.character == -1) CharacterSelectMenu();
      else GameMenu();
    });
  });
  document.getElementById("back").addEventListener("click", function() {
    MainMenu();
  });
}

/**
 * Character selection menu
 */
function CharacterSelectMenu() {
  var card = new Card();
  card.bindClick(function () {
    var battle = new Chifumi();
    battle.start();
  });
  app.updateCallback = function (cursors) {
    if (battlebattle.update(cursors));
  };
}

/**
 * Game home menu
 */
function GameMenu() {
  var card = new Card();
  card.bindClick(function () {
    var battle = new Chifumi();
    battle.start();
  });
  app.updateCallback = function (cursors) {
    if (battlebattle.update(cursors));
  };
}