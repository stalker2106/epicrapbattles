class KeyIndicator {
  constructor(symbol)
  {
    this.symbol = symbol;
  }
}

function request(method, url, callback, data, authorize)
{
  var xhr = new XMLHttpRequest();
  xhr.open(method, url, true);
  xhr.responseType = "json";
  xhr.setRequestHeader("Content-Type", "application/json");
  if (authorize) xhr.setRequestHeader("Authorization", "Bearer "+app.token);
  xhr.onreadystatechange = function() { if (xhr.readyState == 4) callback(xhr); };
  if (method == "POST") xhr.send(data);
  else xhr.send();
}