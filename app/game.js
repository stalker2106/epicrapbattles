/**
 * MAIN GAME ROUTINE
 */

var config = {
  type: Phaser.AUTO,
  width: 800,
  height: 600,
};
var cursors;
var gui = document.getElementById("overlay");
var app = {
  data: {},
  player: undefined,
  token: ""
};

var scene = {
  preload: preload,
  create: create,
  update: update
};
var game = new Phaser.Game(800, 600, Phaser.AUTO, "EpicRapBattles", scene);
gui.style.width = "800px";
gui.style.height = "600px";

/**
 * ENGINE
 */

function preload ()
{
  //System
  //Game
  game.load.spritesheet("chifumi", "assets/img/chifumi.png", 104, 106);
  game.load.spritesheet("card", "assets/img/card.png", 750, 1050);
}

function create ()
{
  cursors = this.input.keyboard.createCursorKeys(); //InputManager
  MainMenu(game);
}

function update ()
{
  if (app.updateCallback) app.updateCallback(cursors);
}

/**
 * BEHAVIOUR
 */

function startGame() {
  app.currentBattle = new Chifumi(game);
  app.currentBattle.begin();
}

function downloadGameData() {
  request("GET", "http://localhost:1337/characters", function(xhr) {
    if (xhr.status == 200)
    {
      app.data.characters = xhr.response;
      return (true);
    }
    else
    {
      console.log("Could not load game data: Network error.");
    }
  }, null, true);
  return (false);
}

/**
 * Log/Register user on Server
 * @param {String} method either "register" or "login"
 * @param {String} inputLogin login
 * @param {String} inputPassword password 
 */
function postPlayer(method, inputLogin, inputPassword) {
  return new Promise(function (resolve, reject) {
    if (inputLogin.length < 4 || inputPassword.length < 4)
    {
      console.log("Login/Password length must be over 3 characters.");
      return (false);
    }
    request("POST", "http://localhost:1337/"+method, function(xhr) {
      if (xhr.status == 200)
      {
        if (method == "login")
        {
          app.token = xhr.response.access_token;
          app.player = xhr.response.player;
          downloadGameData();
        }
        resolve();
      }
      else
      {
        reject("Request failed: "+xhr.response);
      }
    }, JSON.stringify({
      login: inputLogin,
      password: sha256(inputPassword)
    }));
  });
}