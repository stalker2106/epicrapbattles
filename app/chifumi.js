/**
 * Class that represents a Game of chifumi
 */
class Chifumi {
  constructor(game)
  {
    this.playerHand = new Hand(game, 100, 400);
    this.AIHand = new Hand(game, 400, 400);
    this.playing = false;
    this.statusLabel = game.add.text(16, 16, "", { fontSize: "32px", fill: "#fff" });
  }

  begin() {
    var chifumi = this;
    this.AIHand.changeStance("none");
    this.playing = true;
    setTimeout(function () {
      chifumi.statusLabel.setText("chi...");
    }, 1000);
    setTimeout(function () {
      chifumi.statusLabel.setText("fu...");
    }, 2000);
    setTimeout(function () {
      chifumi.statusLabel.setText("mi!");
    }, 3000);
    setTimeout(this.end, 4000, this);
  }

  end(chifumi) {
    chifumi.playing = false;
    chifumi.AIHand.randStance();
    switch(Chifumi.getWinner(chifumi.playerHand, chifumi.AIHand))
    {
    case -1:
      chifumi.statusLabel.setText("Draw!");
      setTimeout(function () {
        chifumi.begin();
      }, 2000);
      break;
    case 0:
      chifumi.statusLabel.setText("Player wins!");
      break;
    case 1:
      chifumi.statusLabel.setText("AI wins!");
      break;
    }
  }

  static getWinner(player0, player1) {
    if (player0.stance == player1.stance) return (-1); //Draw
    if (player0.stance == "rock")
    {
      if (player1.stance == "paper") return (1); //1 Wins
      else if (player1.stance == "scissors") return (0); //0 Wins
    }
    else if (player0.stance == "paper")
    {
      if (player1.stance == "scissors") return (1); //1 Wins
      else if (player1.stance == "rock") return (0); //0 Wins
    }
    else if (player0.stance == "scissors")
    {
      if (player1.stance == "rock") return (1); //1 Wins
      else if (player1.stance == "paper") return (0); //0 Wins
    }
  }

  update(cursors) {
    if (!this.playing) return;
    if (cursors.left.isDown)
      this.playerHand.changeStance("rock");
    else if (cursors.down.isDown)
      this.playerHand.changeStance("paper");
    else if (cursors.right.isDown)
      this.playerHand.changeStance("scissors");
  }
}

/**
 * Represents a player of chifumi
 */
class Hand {

  constructor(game, x, y) {
    this.sprite = game.add.sprite(x, y, "chifumi");
    this.sprite.animations.add("rock", [0], 1);
    this.sprite.animations.add("paper", [1], 1);
    this.sprite.animations.add("scissors", [2], 1);
    this.sprite.animations.add("none", [3], 1);
    this.changeStance("rock");
  }

  randStance() {
    switch (Math.floor((Math.random() * 3) + 1))
    {
    case 1:
      this.changeStance("rock");
      break;
    case 2:
      this.changeStance("paper");
      break;
    case 3:
      this.changeStance("scissors");
      break;
    }
  }

  changeStance(stance) {
    this.stance = stance;
    this.sprite.animations.play(stance);
  }
}